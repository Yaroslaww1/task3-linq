﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task3LINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            var queries = new Queries();
            
            Console.WriteLine("Query 1:");
            Console.WriteLine(string.Join(Environment.NewLine, queries.GetTasksCountByProject(GuidsConfig.User1)));
            
            Console.WriteLine("Query 2:");
            Console.WriteLine(string.Join(Environment.NewLine, queries.GetTasksWithShortName(GuidsConfig.User2)));
            
            Console.WriteLine("Query 3:");
            Console.WriteLine(string.Join(Environment.NewLine, queries.GetTasksFinishedIn2020(GuidsConfig.User2)));
            
            Console.WriteLine("Query 4:");
            queries
                .GetTeamsFilteredWithUsersSorted()
                .ForEach(team =>
                {
                    Console.WriteLine($"TeamId {team.id}");
                    Console.WriteLine($"TeamName {team.name}");
                    Console.WriteLine(string.Join(Environment.NewLine, team.users));
                    Console.WriteLine();
                });
            
            Console.WriteLine("Query 5:");
            queries
                .GetUsersSortedWithTasksSorted()
                .ForEach(user =>
                {
                    Console.WriteLine(user.user);
                    Console.WriteLine(string.Join(Environment.NewLine, user.tasks));
                    Console.WriteLine();
                });
            
            Console.WriteLine("Query 6:");
            Console.WriteLine(queries.GetDataStructure1(GuidsConfig.User1));
            
            Console.WriteLine("Query 7:");
            Console.WriteLine(queries.GetDataStructure2(GuidsConfig.Project1));
        }
    }
}