using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Task3LINQ.Entities;

namespace Task3LINQ
{
    public class Queries
    {
        private IEnumerable<UserEntity> _users;
        private IEnumerable<TeamEntity> _teams;
        private IEnumerable<ProjectEntity> _projects;
        private IEnumerable<TaskEntity> _tasks;

        public Queries()
        {
            GenerateTestData();

            UpdateTasksRelations();
            UpdateTeamsRelations();
            UpdateProjectsRelations();
        }

        private void GenerateTestData()
        {
            var user1 = new UserEntity(
                GuidsConfig.User1,
                "User 1",
                DateTime.Now.AddYears(-30),
                DateTime.Now.AddDays(-1),
                GuidsConfig.Team1);
            var user2 = new UserEntity(
                GuidsConfig.User2,
                "User 2",
                DateTime.Now.AddYears(-40),
                DateTime.Now.AddDays(-3),
                GuidsConfig.Team1);
            var user3 = new UserEntity(
                GuidsConfig.User3,
                "User 3",
                DateTime.Now.AddYears(-20),
                DateTime.Now.AddDays(-5),
                GuidsConfig.Team2);
            
            var team1 = new TeamEntity(GuidsConfig.Team1,"Team 1");
            var team2 = new TeamEntity(GuidsConfig.Team2,"Team 2");

            var project1 = new ProjectEntity(
                GuidsConfig.Project1,
                "Project 1", 
                "Veryyyyy loooooooooooooonggggg Project Description 1",
                user1.Id,
                team1.Id,
                DateTime.Now.AddDays(-1));
            var project2 = new ProjectEntity(
                GuidsConfig.Project2,
                "Project 2",
                "Super veryyyyy loooooooooooooonggggg Project Description 2",
                user3.Id,
                team2.Id,
                DateTime.Now.AddDays(-2));

            var task1 = new TaskEntity(
                GuidsConfig.Task1,
                "Task 1", 
                "Description 1",
                DateTime.Now.AddDays(-2),
                DateTime.Now.AddDays(-1),
                project1.Id,
                user1.Id);
            var task2 = new TaskEntity(
                GuidsConfig.Task2,
                "Task 2", 
                "Description 01",
                DateTime.Now.AddDays(-3), 
                DateTime.Now.AddDays(-2), 
                project1.Id, 
                user2.Id);
            var task3 = new TaskEntity(
                GuidsConfig.Task3,
                "Task 3",
                "Description 001",
                DateTime.Now.AddDays(-4), 
                DateTime.Now.AddDays(-3), 
                project1.Id, 
                user2.Id);
            
            var task4 = new TaskEntity(
                GuidsConfig.Task4,
                "Task 4",
                "Description 0001",
                DateTime.Now.AddDays(-5), 
                DateTime.Now.AddYears(-4), 
                project2.Id, 
                user3.Id);
            var task5 = new TaskEntity(
                GuidsConfig.Task5,
                "Task 5",
                "Description 00001",
                DateTime.Now.AddDays(-6), 
                DateTime.Now.AddYears(-5), 
                project2.Id, 
                user3.Id);

            _users = new List<UserEntity> {user1, user2, user3};
            _teams = new List<TeamEntity> {team1, team2};
            _tasks = new List<TaskEntity> {task1, task2, task3, task4, task5};
            _projects = new List<ProjectEntity> {project1, project2};
        }
        
        private void UpdateTasksRelations()
        {
            _tasks = _tasks
                .Join(
                    _users,
                    task => task.PerformerId,
                    user => user.Id,
                    ((task, user) =>
                    {
                        task.Performer = user;
                        return task;
                    }))
                .ToList();
        }
        
        private void UpdateTeamsRelations()
        {
            _teams = _teams
                .Select(team =>
                {
                    var teamUsers = _users
                        .Where(user => user.TeamId == team.Id)
                        .ToList();

                    team.Users = teamUsers;
                    return team;
                })
                .ToList();
        }

        private void UpdateProjectsRelations()
        {
            _projects = _projects
                .Join(
                    _teams,
                    p => p.TeamId,
                    t => t.Id,
                    ((entity, teamEntity) =>
                    {
                        entity.Team = teamEntity;
                        return entity;
                    }))
                .Join(
                    _users,
                    p => p.AuthorId,
                    u => u.Id,
                    ((entity, userEntity) =>
                    {
                        entity.Author = userEntity;
                        return entity;
                    }))
                .Select(project =>
                {
                    var projectTasks = _tasks.Where(task => task.ProjectId == project.Id);
                    project.Tasks = projectTasks;
                    return project;
                })
                .ToList();
        }
        
        // Query 1: Отримати кількість тасків у проекті конкретного користувача (по id) (словник, де key буде проект, а value кількість тасків).
        public Dictionary<Guid, int> GetTasksCountByProject(Guid userId)
        {
            // return GetProjectById(projectId).GetUserTasksCount(userId);
            return _projects
                .Where(project => project.AuthorId == userId)
                .ToDictionary(
                    project => project.Id,
                    project => project.Tasks.Count());
        }
        
        // Query 2: Отримати список тасків, призначених для конкретного користувача (по id), де name таска <45 символів (колекція з тасків).
        public List<TaskEntity> GetTasksWithShortName(Guid userId)
        {
            return _tasks
                .Where(task => task.PerformerId == userId)
                .Where(task => task.Name.Length < 45)
                .ToList();
        }
        
        // Query 3: Отримати список (id, name) з колекції тасків, які виконані (finished) в поточному (2020) році для конкретного користувача (по id).
        public List<(Guid id, string name)> GetTasksFinishedIn2020(Guid userId)
        {
            return _tasks
                .Where(task => task.PerformerId == userId)
                .Where(task => task.FinishedAt?.Year == 2021)
                .Select(task => (task.Id, task.Name))
                .ToList();
        }
        
        // https://stackoverflow.com/a/28444291/13174320
        private int GetDifferenceInYears(DateTime startDate, DateTime endDate)
        {
            int years = endDate.Year - startDate.Year;

            if (startDate.Month == endDate.Month &&
                endDate.Day < startDate.Day
                || endDate.Month < startDate.Month)
            {
                years--;
            }

            return years;
        }
        
        // Query 4: Отримати список (id, ім'я команди і список користувачів) з колекції команд, учасники яких старші 10 років, відсортованих за датою реєстрації користувача за спаданням, а також згрупованих по командах
        public List<(Guid id, string name, List<UserEntity> users)> GetTeamsFilteredWithUsersSorted()
        {
            return _teams
                .Where(team => team.Users
                    .All(user => GetDifferenceInYears(user.BirthDate, DateTime.Now) > 10))
                .Select(team =>
                {
                    var sortedUsers = team.Users
                        .OrderByDescending(user => user.RegisteredAt)
                        .ToList();
                    return (team.Id, team.Name, sortedUsers);
                })
                .ToList();
        }
        
        // Query 5: Отримати список користувачів за алфавітом first_name (по зростанню) з відсортованими tasks по довжині name (за спаданням).
        public List<(UserEntity user, List<TaskEntity> tasks)> GetUsersSortedWithTasksSorted()
        {
            return _users
                .OrderBy(user => user.Name)
                .Select(user =>
                {
                    var userTasks = _tasks
                        .Where(task => task.PerformerId == user.Id)
                        .OrderByDescending(task => task.Name)
                        .ToList();

                    return (user, userTasks);
                })
                .ToList();
        }
        
        // Query 6: Отримати наступну структуру (передати Id користувача в параметри):
        // User
        // Останній проект користувача (за датою створення)
        // Загальна кількість тасків під останнім проектом
        // Загальна кількість незавершених або скасованих тасків для користувача
        // Найтриваліший таск користувача за датою (найраніше створений - найпізніше закінчений)
        //      P.S. - в даному випадку, статус таска не має значення, фільтруємо тільки за датою.
        public (
            UserEntity user,
            ProjectEntity lastProject,
            int lastProjectTasksCount,
            int totalUnFinishedTasksCount,
            TaskEntity longestTask) GetDataStructure1(Guid userId)
        {
            var user = _users.First(user => user.Id == userId);
            var lastProject = _projects
                .Where(project => project.AuthorId == userId)
                .OrderByDescending(project => project.CreatedAt)
                .First();
            var lastProjectTasksCount = lastProject.Tasks.Count();
            var totalUnFinishedTasksCount = _tasks
                .Where(task => task.PerformerId == userId)
                .Count(task => task.FinishedAt == null);
            var longestTask = _tasks
                .Where(task => task.PerformerId == userId)
                .Where(task => task.StartedAt != null && task.FinishedAt != null)
                .OrderByDescending(task => task.FinishedAt - task.StartedAt)
                .ThenBy(task => task.StartedAt)
                .ThenByDescending(task => task.FinishedAt)
                .First();

            return (user, lastProject, lastProjectTasksCount, totalUnFinishedTasksCount, longestTask);
        }
        
        // Query 7: Отримати таку структуру:
        // Проект
        // Найдовший таск проекту (за описом)
        // Найкоротший таск проекту (по імені)
        // Загальна кількість користувачів в команді проекту, де або опис проекту >20 символів, або кількість тасків <3
        public (
            ProjectEntity project,
            TaskEntity longestTaskByDescription,
            TaskEntity shortestTaskByName,
            int usersCountWithLongProjectNameOrSmallTasksCount) GetDataStructure2(Guid projectId)
        {
            var project = _projects.First(project => project.Id == projectId);
            var longestTaskByDescription = project.Tasks
                .OrderByDescending(task => task.Description.Length)
                .First();
            var shortestTaskByName = project.Tasks
                .OrderBy(task => task.Name.Length)
                .First();
            var usersCountWithLongProjectNameOrSmallTasksCount = _projects
                .Where(project => project.Tasks.Count() < 3)
                .Where(project => project.Description.Length > 20)
                .Select(project => project.Team.Users.Count())
                .DefaultIfEmpty(0)
                .Sum();
            
            return (project, longestTaskByDescription, shortestTaskByName, usersCountWithLongProjectNameOrSmallTasksCount);
        }
    }
}