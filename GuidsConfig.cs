using System;

namespace Task3LINQ
{
    public static class GuidsConfig
    {
        public static Guid User1 = Guid.Parse("388CE256-809C-48DF-A050-704EC1D510D0");
        public static Guid User2 = Guid.Parse("388CE256-809C-48DF-A050-704EC1D510D1");
        public static Guid User3 = Guid.Parse("388CE256-809C-48DF-A050-704EC1D510D2");
        
        public static Guid Team1 = Guid.Parse("388CE256-809C-48DF-A050-704EC1D510D3");
        public static Guid Team2 = Guid.Parse("388CE256-809C-48DF-A050-704EC1D510D4");
        
        public static Guid Project1 = Guid.Parse("388CE256-809C-48DF-A050-704EC1D510D5");
        public static Guid Project2 = Guid.Parse("388CE256-809C-48DF-A050-704EC1D510D6");
        
        public static Guid Task1 = Guid.Parse("388CE256-809C-48DF-A050-704EC1D510D7");
        public static Guid Task2 = Guid.Parse("388CE256-809C-48DF-A050-704EC1D510D8");
        public static Guid Task3 = Guid.Parse("388CE256-809C-48DF-A050-704EC1D510D9");
        public static Guid Task4 = Guid.Parse("388CE256-809C-48DF-A050-704EC1D51010");
        public static Guid Task5 = Guid.Parse("388CE256-809C-48DF-A050-704EC1D51011");
    }
}