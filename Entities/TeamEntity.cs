using System;
using System.Collections.Generic;

namespace Task3LINQ.Entities
{
    public class TeamEntity
    {
        public readonly Guid Id;
        public readonly string Name;
        
        public IEnumerable<UserEntity> Users;

        public TeamEntity(Guid id, string name)
        {
            Id = id;
            Name = name;
            
            Users = new List<UserEntity>();
        }
    }
}