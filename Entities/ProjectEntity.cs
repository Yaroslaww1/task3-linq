using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Task3LINQ.Entities
{
    public class ProjectEntity
    {
        public readonly Guid Id;
        public readonly string Name;
        public readonly string Description;
        public DateTime CreatedAt;

        public readonly Guid AuthorId;
        public UserEntity Author;
        
        public readonly Guid TeamId;
        public TeamEntity Team;
        
        public IEnumerable<TaskEntity> Tasks;
        
        public ProjectEntity(
            Guid id,
            string name,
            string description,
            Guid authorId,
            Guid teamId,
            DateTime createdAt)
        {
            Id = id;
            Name = name;
            Description = description;
            CreatedAt = createdAt;

            AuthorId = authorId;
            TeamId = teamId;
            
            Tasks = new List<TaskEntity>();
        }
        
        public override string ToString()
        {
            return $"Project: {Name} {CreatedAt}";
        }
    }
}