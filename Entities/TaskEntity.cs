#nullable enable
using System;

namespace Task3LINQ.Entities
{
    public class TaskEntity
    {
        public readonly Guid Id;
        public readonly string Name;
        public readonly string Description;
        public readonly DateTime? StartedAt;
        public readonly DateTime? FinishedAt;
        
        public readonly Guid ProjectId;
        
        public readonly Guid PerformerId;
        public UserEntity Performer;
        
        public TaskEntity(
            Guid id,
            string name,
            string description,
            DateTime? startedAt,
            DateTime? finishedAt,
            Guid projectId,
            Guid performerId)
        {
            Id = id;
            Name = name;
            Description = description;
            StartedAt = startedAt;
            FinishedAt = finishedAt;
        
            ProjectId = projectId;
            PerformerId = performerId;
        }
        
        public override string ToString()
        {
            return $"Task: {Name}";
        }
    }
}