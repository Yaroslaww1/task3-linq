using System;

namespace Task3LINQ.Entities
{
    public class UserEntity
    {
        public readonly Guid Id;
        public readonly string Name;
        public readonly DateTime BirthDate;
        public readonly DateTime RegisteredAt;
        
        public readonly Guid TeamId;

        public UserEntity(Guid id, string name, DateTime birthDate, DateTime registeredAt, Guid teamId)
        {
            Id = id;
            Name = name;
            BirthDate = birthDate;
            RegisteredAt = registeredAt;

            TeamId = teamId;
        }

        public override string ToString()
        {
            return $"User: {Name} {BirthDate} {RegisteredAt}";
        }
    }
}